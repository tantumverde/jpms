# DEMO - Single module application #

```
C:.
│   .gitignore
│   README.md
└───jpmsdemo-single-module
    └───src
        └───com.tsystems.jpmshello
            │   module-info.java
            │
            └───com
                └───tsystems
                    └───jpmshello
                            HelloModules.java
```
### module-info.java ###
```
module com.mydeveloperplanet.jpmshello {
    requires java.base;
}
```

## Compilation ##
>**You should be in the folder jpmsdemo-single-module to run the following commands!**
```
javac -d mods/com.tsystems.jpmshello src/com.tsystems.jpmshello/module-info.java src/com.tsystems.jpmshello/com/tsystems/jpmshello/HelloModules.java
```
## Execution ##
```
java --module-path mods --module com.tsystems.jpmshello/com.tsystems.jpmshello.HelloModules
```
## Create a JAR File ##
```
jar --create --file target/jpms-hello-modules.jar --main-class com.tsystems.jpmshello.HelloModules -C mods/com.tsystems.jpmshello .
```
### Now we can execute the application from the JAR file: ###

```
java --module-path target/jpms-hello-modules.jar --module com.tsystems.jpmshello/com.tsystems.jpmshello.HelloModules
```
### We can also check the module description as we do with the Java standard modules: ###
```
java --module-path target/jpms-hello-modules.jar --describe-module com.tsystems.jpmshello
```